<?php
/**
 * Created by PhpStorm.
 * User: emrevural
 * Date: 1.08.2017
 * Time: 11:04
 */

namespace Service\ServiceBundle\CommonServices;



use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Curl
{
//    use ServicesTrait;
//    use ControllerTrait;
    protected $_useragent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1';
    protected $_url;
    protected $_followlocation;
    protected $_timeout;
    protected $_maxRedirects;
    protected $_cookieFileLocation = './cookie.txt';
    protected $_post = false;
    protected $_postFields = null;
    protected $_file;
    protected $_info;
    protected $_method = "GET";
    protected $_binary = false;
    protected $_referer = "http://www.google.com";

    protected $_session;
    protected $_webpage;
    protected $_includeHeader;
    protected $_noBody;
    protected $_status;
    protected $_binaryTransfer;
    protected $_header;
    protected $_error_no;
    public $authentication = 0;
    public $auth_name = '';
    public $auth_pass = '';
    public $ip = null;

//    public $container;


    public function __construct(ContainerInterface $container, $followlocation = false, $timeOut = 3600, $maxRedirecs = 4, $binaryTransfer = false, $includeHeader = false, $noBody = false)
    {

        /** service trait içerinden geliyor*/
        $this->container = $container;


//        $this->_url = $url;
        $this->_post = false;
        $this->_followlocation = $followlocation;
        $this->_timeout = $timeOut;
        $this->_maxRedirects = $maxRedirecs;
        $this->_noBody = $noBody;
        $this->_includeHeader = $includeHeader;
        $this->_binaryTransfer = $binaryTransfer;

        $this->_cookieFileLocation = dirname(__FILE__) . '/cookie.txt';

    }

    public function useAuth($use)
    {
        $this->authentication = 0;
        if ($use == true) $this->authentication = 1;
    }

    public function setName($name)
    {
        $this->auth_name = $name;
    }

    public function setPass($pass)
    {
        $this->auth_pass = $pass;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }


    public function setContentType($type = "json")
    {

        if ($type == "json") {

            $this->addHeader("Content-Type: application/json;charset=utf-8");

        } elseif ($type == "urlencoded") {
            $this->addHeader("Content-Type: application/x-www-form-urlencoded;charset=utf-8");
        }

    }


    public function addHeader($string)
    {

        array_push($this->_header, $string);
    }

    public function setReferer($referer)
    {
        $this->_referer = $referer;
    }

    public function setCookiFileLocation($path)
    {
        $this->_cookieFileLocation = $path;
    }

    /**
     * @example [key=>"value"] , key=value&key2=value2
     * @param $postFields
     */
    public function setPost($postFields)
    {
        $this->_post = true;
        $this->_method = "POST";
        $isJson = false;
        try {
            json_decode($postFields);

            $isJson = true;
        } catch (\Exception $e) {

        }


        if (is_array($postFields)) {

            $this->setContentType("urlencoded");
            $this->_postFields = http_build_query($postFields);

        } elseif ($isJson) {

            $this->setContentType("json");
            $this->_postFields = $postFields;

        } else {
            $this->setContentType("urlencoded");

            $this->_postFields = $postFields;

        }
    }

    public function setDelete()
    {
        $this->_method = "DELETE";


    }

    public function setFile($fp)
    {

        $this->_file = $fp;

    }

    public function setBinary()
    {

        $this->_binary = true;

    }

    public function setUserAgent($userAgent)
    {
        $this->_useragent = $userAgent;
    }

    public function createCurl($url = 'nul', $close = true)
    {


        if ($url != 'nul') {
            $this->_url = $url;
        }

        $s = curl_init();

//        dump($this->_header);
//        exit;


        curl_setopt($s, CURLOPT_URL, $this->_url);
//        curl_setopt($s, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/x-www-form-urlencoded'
//        ));

        curl_setopt($s, CURLOPT_HTTPHEADER, $this->_header);
//        curl_setopt($s, CURLOPT_HTTPHEADER, array('Expect:'));
//        curl_setopt($s, CURLOPT_FILE, $this->_file);
//        curl_setopt($s, CURLOPT_FOLLOWLOCATION, true);


        curl_setopt($s, CURLOPT_PORT, "8080");
        curl_setopt($s, CURLOPT_TIMEOUT, 3600);
        curl_setopt($s, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($s, CURLOPT_ENCODING, "");
        curl_setopt($s, CURLOPT_MAXREDIRS, $this->_maxRedirects);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($s, CURLOPT_FOLLOWLOCATION, $this->_followlocation);
        curl_setopt($s, CURLOPT_COOKIEJAR, $this->_cookieFileLocation);
        curl_setopt($s, CURLOPT_COOKIEFILE, $this->_cookieFileLocation);

        if ($this->ip != null) {
            curl_setopt($s, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $this->ip", "HTTP_X_FORWARDED_FOR: $this->ip",));
        }

        if ($this->_binary) {
            curl_setopt($s, CURLOPT_BINARYTRANSFER, 1);
        }
        if ($this->authentication == 1) {
            curl_setopt($s, CURLOPT_USERPWD, $this->auth_name . ':' . $this->auth_pass);
        }
        if ($this->_post) {
            curl_setopt($s, CURLOPT_POST, true);
            curl_setopt($s, CURLOPT_POSTFIELDS, $this->_postFields);
        }

        curl_setopt($s, CURLOPT_CUSTOMREQUEST, $this->_method);

        if ($this->_includeHeader) {
            curl_setopt($s, CURLOPT_HEADER, true);
        }

        if ($this->_noBody) {
            curl_setopt($s, CURLOPT_NOBODY, true);
        }
        /*
        if($this->_binary)
        {
            curl_setopt($s,CURLOPT_BINARYTRANSFER,true);
        }
        */
        curl_setopt($s, CURLOPT_USERAGENT, $this->_useragent);
        curl_setopt($s, CURLOPT_REFERER, $this->_referer);

//        dump($s);
//        exit;
        $this->_webpage = curl_exec($s);
        $this->_status = curl_getinfo($s, CURLINFO_HTTP_CODE);


        if (curl_errno($s)) {

            $this->_error_no = curl_error($s);
            $this->curlErrorControl();
        }


        if ($close) {
            curl_close($s);

        }

        $this->statusControl();

        if ($close) {

//            $this->reset()
        }

        return $this->_webpage;
    }


    public function getHttpStatus()
    {
        return $this->_status;
    }

    public function __tostring()
    {
        return $this->_webpage;
    }

    private function statusControl()
    {



        $request = $this->container->get("request_stack")->getCurrentRequest();
        $session = $this->container->get("session");
        $isAjax = $request->isXmlHttpRequest();

        //TODO http kodlara göre bir raporlama

        if ($this->_status == 401) {


        } else if ($this->_status == 500) {


        } else if ($this->_status == 404) {


        } else if ($this->_status != 200) {


        }

    }

    private function curlErrorControl()
    {

        //TODO curl errorlarına göre bir raporlama
    }

}
