<?php
/**
 * Created by PhpStorm.
 * User: emrevural
 * Date: 18.07.17
 * Time: 21:34
 */

namespace Service\ServiceBundle\Controller;



use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


abstract class CoreService
{


    protected $container;


    public function __construct(ContainerInterface $container = null)
    {

        if($container == null){

            //TODO burada error
        }

        /** Container**/
        $this->container = $container;


    }


    public function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }

    public function redirectToRoute($route, array $parameters = array(), $status = 302)
    {
        return $this->redirect($this->getRouter()->generate($route, $parameters), $status);
    }

    public function getUserManager(){

        return $this->container->get("fos_user.user_manager");

    }

    public function getRequest(){

        return $this->container->get("request_stack")->getCurrentRequest();

    }

    public function getUser(){

        $container = $this->container;
        /** Giriş yapan kullanıcı bilgileri **/
        $token = $container->get("security.token_storage")->getToken();
        $user = $token->getUser();

        return $user;
    }

    public function getRoles(){

        $user = $this->getUser();

        $roles = $user->getRoles();

        return $roles;
    }

    public function getEntityManager(){

        $isDebug = $this->isDebug();
        if($isDebug == false){

            $em = $this->container->get("doctrine")->getManager();
        }else{
            $em = $this->container->get("doctrine")->getEntityManager();
        }

        return $em;
    }

    public function isDebug(){

        $kernel = $this->getKernel();

        return $kernel->isDebug();
    }

    public function getKernel(){

        return $this->container->get('kernel');
    }

    public function getEnvironment(){

        $kernel = $this->getKernel();
        $environment = $kernel->getEnvironment();

        return $environment;
    }

    public function getRouter(){
        return $this->container->get("router");
    }

}