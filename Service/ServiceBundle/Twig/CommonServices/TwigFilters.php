<?php
/**
 * Created by PhpStorm.
 * User: emrevural
 * Date: 29.05.2018
 * Time: 12:45
 */

namespace Service\ServiceBundle\Twig\CommonServices;


use Symfony\Component\DependencyInjection\ContainerInterface;

class TwigFilters extends \Twig_Extension
{

    public $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

}