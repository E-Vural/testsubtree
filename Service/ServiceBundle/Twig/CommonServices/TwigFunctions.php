<?php
/**
 * Created by PhpStorm.
 * User: emrevural
 * Date: 29.05.2018
 * Time: 12:42
 */

namespace Service\ServiceBundle\Twig\CommonServices;


use Symfony\Component\DependencyInjection\ContainerInterface;

class TwigFunctions extends \Twig_Extension
{

    public $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getParameter', array($this, 'getParameter')),
        );
    }

    public function getParameter($parameter)
    {

        return $this->container->getParameter($parameter);

    }





}